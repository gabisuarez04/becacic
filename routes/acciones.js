const express = require('express');
const router = express.Router();
const accionesController = require('../controllers/acciones');

router.route('/')
    .get(accionesController.getAcciones)
    
router.route('/:nodo&:sector&:accion')
	.post(accionesController.postAccion);

router.route('/:id')
    .delete(accionesController.deleteAccion);

module.exports = router;