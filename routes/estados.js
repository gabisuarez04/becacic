const express = require('express');
const router = express.Router();
const estadosController = require('../controllers/estados');

router.route('/')
    .get(estadosController.getEstados)
    
router.route('/:nodo&:sector&:estado')
	.post(estadosController.postEstado);

router.route('/:id')
    .delete(estadosController.deleteEstado);

module.exports = router;