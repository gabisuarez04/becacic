const express = require('express');
const router = express.Router();
const sectoresController = require('../controllers/sectores');

router.route('/')
    .get(sectoresController.getSectores)
    .post(sectoresController.postSector);

router.route('/:id')
 //   .get(sectoresController.getSector)
    .put(sectoresController.putSector)
    .delete(sectoresController.deleteSector);

module.exports = router;
