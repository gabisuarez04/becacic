const express = require('express');
const router = express.Router();
const sensoresController = require('../controllers/sensores');

router.route('/')
	.get(sensoresController.getSensores)
	.post(sensoresController.postSensor);

router.route('/:id')
	.put(sensoresController.putSensor)
    .delete(sensoresController.deleteSensor);

module.exports = router;