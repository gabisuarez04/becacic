const express = require('express');
const router = express.Router();
const medicionesController = require('../controllers/mediciones');

router.route('/')
    .get(medicionesController.getMediciones)
    
router.route('/:sensor&:valor&:unidad')
	.post(medicionesController.postMedicion);

router.route('/:id')
    .delete(medicionesController.deleteMedicion);

module.exports = router;