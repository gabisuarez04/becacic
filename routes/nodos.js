const express = require('express');
const router = express.Router();
const nodosController = require('../controllers/nodos');

router.route('/')
	.get(nodosController.getNodos)
	.post(nodosController.postNodo);

router.route('/:id')
    .delete(nodosController.deleteNodo);

module.exports = router;