const express = require('express');
const app = express();
const morgan = require('morgan');
const mongoose = require('mongoose');
const config = require('./config');
const bodyParser = require('body-parser');
require('dotenv').config();

// Conexion a la base de datos
const options = {
	//autoIndex: false, --> en produccion ponerlo en false
	keepAlive: 10000,
//	poolSize: 10,
	connectTimeoutMS: 10000, // tiempo limite de espera para que se efectue la conexion
	socketTimeoutMS: 10000,
	connectWithNoPrimary: true, // permite que se conecte a la replica por mas de que el primario no se encuentre disponible
	useNewUrlParser: true 
}; /* en options se mantienen las configuraciones que son iguales en desarrollo y en produccion */

var urlConnection = config.Mongo.client + "://" + config.Mongo.host + ":" + config.Mongo.port + "/" + config.Mongo.dbName;
/* en la url se especifican las opciones que difieren en desarrollo y en produccion */

mongoose.connect(urlConnection,options).then(
	() => { console.log("CONEXION EXITOSA A LA BASE DE DATOS");},
	err => { next(err); }
);


// Paquete de logs
app.use(morgan('dev'));

// Paquete para parsear bodys de los requests 
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());


// Definicion de rutas

//const nodosRoutes = require('./routes/nodos');

const accionesRoutes = require('./routes/acciones');
const estadosRoutes = require('./routes/estados');
const medicionesRoutes = require('./routes/mediciones');
const sectoresRoutes = require('./routes/sectores');
const sensoresRoutes = require('./routes/sensores');
const unidadesRoutes = require('./routes/unidades');

//app.use('/nodos', nodosRoutes);

app.use('/acciones', accionesRoutes);
app.use('/estados', estadosRoutes);
app.use('/mediciones', medicionesRoutes);
app.use('/sectores', sectoresRoutes);
app.use('/sensores', sensoresRoutes); 
app.use('/unidades', unidadesRoutes);


// Manejador de request no definidos; si se llego a este punto es porque al request no lo puede manejar ningun router
app.use((req, res, next) => {
	const error = new Error('Not Found');
	error.status = 404;
	next(error); // Hace que se ejecute el manejador de errores
});

// Manejador de errores de toda la app
app.use((error, req, res, next) => {
	res.status(error.status || 500);
	res.json({
			error: {
				message: error.message
			}	
		}
	
	);
}

);

module.exports = app;
