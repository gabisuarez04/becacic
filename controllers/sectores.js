
const Sector = require('../models/sector');

exports.getSectores = function (req, res, next) { 
		Sector.find()
			.exec()
			.then(sectores => {
				console.log(sectores);
				return res.json({sectores: sectores});
			})
			.catch(err => {
				console.log(err);
				return next(err);
			});
		
};

exports.postSector = function (req, res, next) { 

    // la interfaz restringe la unidad posible o se valida si existe en la base 
    var sector = new Sector({
        nivel: req.body.nivel,
		codigo: req.body.codigo,
        area: req.body.area,
        unidad: req.body.unidad	    
	});
    sector.save()
			.then(sector => {
				console.log('Sector añadido con éxito');		
				return res.json({message: 'Sector añadido', sector: sector});
			})
			.catch(err => {
				console.log('Error al añadir el sector');
				console.log("ERROR: " + err);
				return next(err);
			});
};

exports.putSector = function (req, res, next){

    // la interfaz restringe la unidad posible o se valida si existe en la base 
	sector = new Sector({
        nivel: req.body, nivel, 
		codigo: req.body.codigo,
        area: req.body.area,
        unidad: req.body.unidad	 
	});
	Sector.findByIdAndUpdate(req.params.id, sector)
			.exec()
			.then(sector => { 
					if (!sector){
						console.log('No existe el sector con id:', req.params.id);		
						return res.json({message: 'No existe el sector con id: '+req.params.id});
					}
					console.log('Sector actualizado con éxito');
					return res.json({message: 'Actualizacion exitosa del sector con id: '+req.params.id});
			})
			.catch(err => {
				console.log('Error al actualizar el sector: ',req.params.id);
				return next(err); 
			});
};


exports.deleteSector = function (req, res, next) {

	Sector.findByIdAndRemove(req.params.id)
			.exec()
			.then(sector => {
				if (!sector){
					console.log('No existe el sector con id:', req.params.id);		
					return res.json({message: 'No existe el sector con id: '+req.params.id});
				}
				console.log('Sector eliminado con éxito');		
				return res.json({message: 'Sector eliminado', sector: sector});
			})
			.catch(err => {
				console.log('Error al eliminar el sector con id: ',req.params.id);
				return next(err); 
			});


};
