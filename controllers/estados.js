const Estado = require('../models/estado');

exports.getEstados = function (req, res, next) { 
		Estado.find()
			.exec()
			.then(estados => {
				console.log(estados);
				return res.json({estados: estados});
			})
			.catch(err => {
				console.log(err);
				return next(err);
			});
		
};

exports.postEstado = function (req, res, next) { 
	var nDate = new Date().toLocaleString('es-AR', {
		timeZone: 'America/Argentina/Buenos_Aires'
	  });
	var estado = new Estado({
		fecha: nDate, 
        nodo: req.params.nodo,
        sector: req.params.sector,
        estado: req.params.estado
	});
    estado.save()
			.then(estado => {
				console.log('Estado añadida con éxito');		
            	return res.json({message: 'Estado añadido', estado: estado});
			})
			.catch(err => {
				console.log('Error al añadir el estado');
				console.log("ERROR: " + err);
                return res.json({respuesta: 'error'});
            });
    
};

exports.putEstado = function (req, res, next){
	estado = new Estado({
		nodo: req.params.nodo,
        sector: req.params.sector,
        estado: req.params.estado,
        descripcion: req.params.descripcion
	});
	Estado.findByIdAndUpdate(req.params.id, estado)
			.exec()
			.then(estado => { 
					if (!estado){
						console.log('No existe el estado con id:', req.params.id);		
						return res.json({message: 'No existe el estado con id: '+req.params.id});
					}
					console.log('estado actualizado con éxito');
					return res.json({message: 'Actualizacion exitosa del estado con id: '+req.params.id});
			})
			.catch(err => {
				console.log('Error al actualizar el estado:',req.params.id);
				return next(err); 
			});
};


exports.deleteEstado = function (req, res, next) {

	Estado.findByIdAndRemove(req.params.id)
			.exec()
			.then(estado => {
				if (!estado){
					console.log('No existe el estado con id:', req.params.id);		
					return res.json({message: 'No existe el estado con id: '+req.params.id});
				}
				console.log('Estado eliminado con éxito');		
				return res.json({message: 'Estado eliminado', estado: estado});
			})
			.catch(err => {
				console.log('Error al eliminar el estado con id:',req.params.id);
				return next(err); 
			});


};
