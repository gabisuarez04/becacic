
const Unidad = require('../models/unidad');

exports.getUnidades = function (req, res, next) { 
		Unidad.find()
			.exec()
			.then(unidades => {
				console.log(unidades);
				return res.json({unidades: unidades});
			})
			.catch(err => {
				console.log(err);
				return next(err);
			});
		
};

exports.postUnidad = function (req, res, next) { 

	var unidad = new Unidad({
		tipo: req.body.tipo,
		alias: req.body.alias	    
	});
    unidad.save()
			.then(unidad => {
				console.log('Unidad añadida con éxito');		
				return res.json({message: 'Unidad añadida', unidad: unidad});
			})
			.catch(err => {
				console.log('Error al añadir la unidad');
				console.log("ERROR: " + err);
				return next(err);
			});
};

exports.putUnidad = function (req, res, next){
	unidad = new Unidad({
		tipo: req.body.tipo,
		alias: req.body.alias
	});
	Unidad.findByIdAndUpdate(req.params.id, unidad)
			.exec()
			.then(unidad => { 
					if (!unidad){
						console.log('No existe la unidad con id:', req.params.id);		
						return res.json({message: 'No existe la unidad con id: '+req.params.id});
					}
					console.log('Unidad actualizada con éxito');
					return res.json({message: 'Actualizacion exitosa de la Unidad con id: '+req.params.id});
			})
			.catch(err => {
				console.log('Error al actualizar la unidad:',req.params.id);
				return next(err); 
			});
};


exports.deleteUnidad = function (req, res, next) {

	Unidad.findByIdAndRemove(req.params.id)
			.exec()
			.then(unidad => {
				if (!unidad){
					console.log('No existe la unidad con id:', req.params.id);		
					return res.json({message: 'No existe la unidad con id: '+req.params.id});
				}
				console.log('Unidad eliminada con éxito');		
				return res.json({message: 'Unidad eliminada', unidad: unidad});
			})
			.catch(err => {
				console.log('Error al eliminar la unidad con id:',req.params.id);
				return next(err); 
			});


};
