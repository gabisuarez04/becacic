const Accion = require('../models/accion');

exports.getAcciones = function (req, res, next) { 
		Accion.find()
			.exec()
			.then(acciones => {
				console.log(acciones);
				return res.json({acciones: acciones});
			})
			.catch(err => {
				console.log(err);
				return next(err);
			});
		
};

exports.postAccion = function (req, res, next) { 
	var nDate = new Date().toLocaleString('es-AR', {
		timeZone: 'America/Argentina/Buenos_Aires'
	  });
	var accion = new Accion({
		fecha: nDate, 
        nodo: req.params.nodo,
        sector: req.params.sector,
        accion: req.params.accion
	});
    accion.save()
			.then(accion => {
				console.log('Accion añadida con éxito');		
            	return res.json({message: 'Accion añadida', accion: accion});
			})
			.catch(err => {
				console.log('Error al añadir la accion');
				console.log("ERROR: " + err);
                return res.json({respuesta: 'error'});
            });
    
};

exports.putAccion = function (req, res, next){
	accion = new Accion({
		nodo: req.params.nodo,
        sector: req.params.sector,
        accion: req.params.accion,
        descripcion: req.params.descripcion
	});
	Accion.findByIdAndUpdate(req.params.id, accion)
			.exec()
			.then(accion => { 
					if (!accion){
						console.log('No existe la accion con id:', req.params.id);		
						return res.json({message: 'No existe la accion con id: '+req.params.id});
					}
					console.log('accion actualizada con éxito');
					return res.json({message: 'Actualizacion exitosa de la accion con id: '+req.params.id});
			})
			.catch(err => {
				console.log('Error al actualizar la accion:',req.params.id);
				return next(err); 
			});
};


exports.deleteAccion = function (req, res, next) {

	Accion.findByIdAndRemove(req.params.id)
			.exec()
			.then(accion => {
				if (!accion){
					console.log('No existe la accion con id:', req.params.id);		
					return res.json({message: 'No existe la accion con id: '+req.params.id});
				}
				console.log('Accion eliminada con éxito');		
				return res.json({message: 'Accion eliminada', accion: accion});
			})
			.catch(err => {
				console.log('Error al eliminar la accion con id:',req.params.id);
				return next(err); 
			});


};
