const Medicion = require('../models/medicion');

exports.getMediciones = function (req, res, next) { 
		Medicion.find()
			.exec()
			.then(mediciones => {
				console.log(mediciones);
				return res.json({mediciones: mediciones});
			})
			.catch(err => {
				console.log(err);
				return next(err);
			});
		
};

exports.postMedicion = function (req, res, next) { 
	var nDate = new Date().toLocaleString('es-AR', {
		timeZone: 'America/Argentina/Buenos_Aires'
	  });
	var medicion = new Medicion({
		fecha: nDate,
        sensor: req.params.sensor,
        valor: req.params.valor,
        unidad: req.params.unidad
	});
    medicion.save()
			.then(medicion => {
				console.log('Medicion añadida con éxito');		
            	return res.json({message: 'Medicion añadida', medicion: medicion});
            //  return res.json({respuesta: processData()});
			})
			.catch(err => {
				console.log('Error al añadir la medicion');
				console.log("ERROR: " + err);
            //	return next(err);
                return res.json({respuesta: 'error'});
            });
    
};

exports.putMedicion = function (req, res, next){
	medicion = new Medicion({
		tipo: req.body.tipo,
		alias: req.body.alias
	});
	Medicion.findByIdAndUpdate(req.params.id, medicion)
			.exec()
			.then(medicion => { 
					if (!medicion){
						console.log('No existe la medicion con id:', req.params.id);		
						return res.json({message: 'No existe la medicion con id: '+req.params.id});
					}
					console.log('medicion actualizada con éxito');
					return res.json({message: 'Actualizacion exitosa de la medicion con id: '+req.params.id});
			})
			.catch(err => {
				console.log('Error al actualizar la medicion:',req.params.id);
				return next(err); 
			});
};


exports.deleteMedicion = function (req, res, next) {

	Medicion.findByIdAndRemove(req.params.id)
			.exec()
			.then(medicion => {
				if (!medicion){
					console.log('No existe la medicion con id:', req.params.id);		
					return res.json({message: 'No existe la medicion con id: '+req.params.id});
				}
				console.log('Medicion eliminada con éxito');		
				return res.json({message: 'Medicion eliminada', medicion: medicion});
			})
			.catch(err => {
				console.log('Error al eliminar la medicion con id:',req.params.id);
				return next(err); 
			});


};

processData = function(){
    return 'error';
}