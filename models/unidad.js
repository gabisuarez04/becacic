const mongoose = require('mongoose');
const autoIncrement = require('mongoose-plugin-autoinc');

const unidadSchema = mongoose.Schema(
{
    tipo: String,
    alias: String
}
);

unidadSchema.plugin(autoIncrement.plugin, 'unidad');
module.exports = mongoose.model('unidad', unidadSchema, 'unidades');
