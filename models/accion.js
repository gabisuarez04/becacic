const mongoose = require('mongoose');
const autoIncrement = require('mongoose-plugin-autoinc');

const accionSchema = mongoose.Schema(
{
    fecha: { type: String },
    nodo: { type: Number, ref: 'nodo'},
	sector: { type: Number, ref:'sector' },
    accion: { type: String }
	
}
);

accionSchema.plugin(autoIncrement.plugin, 'accion');
module.exports = mongoose.model('accion', accionSchema, 'acciones');