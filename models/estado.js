const mongoose = require('mongoose');
const autoIncrement = require('mongoose-plugin-autoinc');

const estadoSchema = mongoose.Schema(
{
    fecha: { type: String },
    nodo: { type: Number, ref: 'nodo'},
	sector: { type: Number, ref:'sector' },
    estado: { type: String }
	
}
);

estadoSchema.plugin(autoIncrement.plugin, 'estado');
module.exports = mongoose.model('estado', estadoSchema, 'estados');