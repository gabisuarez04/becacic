const mongoose = require('mongoose');
const autoIncrement = require('mongoose-plugin-autoinc');

const nodoSchema = mongoose.Schema(
{
    mac: String, 
    alias: String,
    detalle: String,
    sector: Number,
    sensores: [ { id: { type:Number, ref: 'sensor'} } ]   
}
);

nodoSchema.plugin(autoIncrement.plugin, 'nodo');
module.exports = mongoose.model('nodo', nodoSchema, 'nodos');
