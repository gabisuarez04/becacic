const mongoose = require('mongoose');
const autoIncrement = require('mongoose-plugin-autoinc');

const sectorSchema = mongoose.Schema(
{
    nivel: String,
    codigo: String,
    area: Number,
    unidad: { type: Number, ref: 'unidad'}
}
);

sectorSchema.index({nivel: 1, codigo: 1}, {unique: true});
sectorSchema.plugin(autoIncrement.plugin, 'sector');
module.exports = mongoose.model('sector', sectorSchema, 'sectores');
