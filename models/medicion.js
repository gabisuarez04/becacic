const mongoose = require('mongoose');
const autoIncrement = require('mongoose-plugin-autoinc');

const medicionSchema = mongoose.Schema(
{
    fecha: { type: String },
    nodo: { type: Number, ref: 'nodo'},
	sector: { type: Number, ref:'sector' },
    sensor: { type: Number, ref: 'sensor' },
    valor: { type: Number },
    unidad: { type: Number, ref: 'unidad'}
	
}
);


medicionSchema.plugin(autoIncrement.plugin, 'medicion');
module.exports = mongoose.model('medicion', medicionSchema, 'mediciones');